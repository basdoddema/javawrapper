# Java Weka Application
 
## Purpose
A Java application that can classify unknown instances of wine samples that are provided by the user.  
The application can be used with either a file or a line with physicochemical wine properties.  
The prediction is based on the properties of the wine and uses a model that has been made in Weka.  
 
## Installation
To execute this application, Java version 11.0.0 or higher must be installed.
 
## Usage
- This application can only be executed on the command line.
- Make sure that the input data is min-max normalized before using it in the application.  
- To execute the application, clone this repository to your pc.
- Make sure that you change your directory to the `WekaRunner/builds/libs` folder.
  
When you are in the right folder, the application can be executed in multiple ways:  
Firstly the help can be called, the syntax for calling the help is:   
 
`$ java -jar WekaWrapper-Jar-1.0-SNAPSHOT.jar -help`  
 
The application can be executed with a csv file with unclassified instances.   
The syntax for this way of executing the application is:  
 
`$ java -jar WekaWrapper-Jar-1.0-SNAPSHOT.jar -f "file path to input file with unknown instances"`  
 
The output of the application when using a file will look like this:  
 
`@data
0.263636,0.273973,0.210526,0.322581,0.388298,0.18239,0.512567,0.452174,0.432836,0.196429,Bad
0.318182,0.178082,0.368421,0.387097,0.287234,0.150943,0.512567,0.321739,0.597015,0.232143,Good`  
 
To execute the application with a line with comma separated data, this syntax must be used:  
`$ java -jar WekaWrapper-Jar-1.0-SNAPSHOT.jar -l "your comma separated line with wine properties"`  
 
The output of the application when using a line with data will look like this:
 
`@data
0.054545,0.136986,0.328947,0.290323,0.484043,0.27673,0.428789,0.452174,0.328358,0.142857,Bad`
 
All the numbers are the properties that are given in the csv file and the "Bad" or "Good" at the end of the line 
is the label that has been classified by the application.  
 
 


