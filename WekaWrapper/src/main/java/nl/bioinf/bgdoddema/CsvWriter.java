package nl.bioinf.bgdoddema;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * this class writes a csv file that can be used in the WekaRunner class
 *
 * @author Bas
 */
public class CsvWriter {

    /**
     * writes a csv file from a single line with properties
     *
     * @param line single line with properties
     */
    public CsvWriter(String line) {
        String filePath = "../../testdata/line_data.csv";
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(filePath));
            //writing data to a csv file
            String[] attributes = {"fixed.acidity", "volatile.acidity", "citric.acid", "residual.sugar",
                    "chlorides", "total.sulfur.dioxide", "density", "pH", "sulphates", "alcohol"};

            String[] values = line.split(",");

            //instantiating the List Object
            List<String[]> list = new ArrayList<>();
            list.add(attributes);
            list.add(values);

            //writing data to the csv file
            writer.writeAll(list);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * returns the file path where the output file is located
     *
     * @return file path
     */
    public String getFilePath() {
        return "../../testdata/line_data.csv";
    }

}
