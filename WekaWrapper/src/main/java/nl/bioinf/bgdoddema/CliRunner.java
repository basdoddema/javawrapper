package nl.bioinf.bgdoddema;

import org.apache.commons.cli.MissingArgumentException;

import java.util.Arrays;

/**
 * Main class that uses command line arguments that are parsed by Apache CLI.
 *
 * @author Bas
 */
public final class CliRunner {

    /**
     * private constructor because this class is not supposed to be instantiated.
     */
    private CliRunner() {
    }

    /**
     * @param args CL arguments
     */
    public static void main(final String[] args) {
        try {
        CliOptionsProvider op = new CliOptionsProvider((args));
        if (args.length == 0) {
            throw new MissingArgumentException("No arguments given");
        }
        if (op.helpRequested()) {
            op.printHelp();
        }
        // catch if no arguments are given
        } catch (IllegalStateException | MissingArgumentException ex) {
            System.err.println("Something went wrong while processing your command line");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            CliOptionsProvider op = new CliOptionsProvider(new String[]{});
            op.printHelp();
        }
    }
}