package nl.bioinf.bgdoddema;

/**
 * interface that serves the options for the application.
 *
 * @author Bas
 */
public interface OptionsProvider {
    /**
     * serves the file that the user want to classify.
     * @return File the file
     */
    String getFile();
    /**
     * serves the line that the user want to classify.
     * @return Line the line with attributes
     */
    String getLine();
}
