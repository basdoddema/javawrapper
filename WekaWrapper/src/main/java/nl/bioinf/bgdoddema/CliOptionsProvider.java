package nl.bioinf.bgdoddema;

import org.apache.commons.cli.*;

/**
 * class that provides all the options for the args arguments,
 * also checks if all the input is legal, if not exceptions are given.
 *
 * @author Bas
 */
public class CliOptionsProvider implements OptionsProvider {
    private static final String HELP = "help";
    private static final String FILE = "file";
    private static final String LINE = "line";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;

    /**
     * provides args parameter for application
     *
     * @param args
     */
    public CliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * process command line and initialize options
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * check if help was requested; if so, return true.
     *
     * @return helpRequested
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * build option objects
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message");
        Option fileOption = new Option("f", FILE, true, "CSV file with comma separated instances, " +
                "these instances should contain min-max normalized data that the user wants to classify");
        Option lineOption = new Option("l", LINE, true, "Comma separated line with a single " +
                "instance, this instance should contain min-max normalized data that the user wants to classify");
        options.addOption(helpOption);
        options.addOption(fileOption);
        options.addOption(lineOption);
    }

    /**
     * process CL arguments
     */
    private void processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);
            if (commandLine.hasOption(FILE)) {
                String file = commandLine.getOptionValue(FILE);
                // check if file extension is legal
                if (file.endsWith(".csv")) {
                    WekaRunner wr = new WekaRunner();
                    wr.startFile(file);
                } else {
                    throw new IllegalArgumentException("File argument is not legal: Please use a csv file");
                }
            } else if (commandLine.hasOption(LINE)) {
                String line = commandLine.getOptionValue(LINE);
                // check if line content is legal
                if (line.matches("[0-9.,]+")) {
                    if (line.split(",").length == 10) {
                        // create csv file from line attributes
                        CsvWriter writer = new CsvWriter(line);
                        // get file path where the file is located
                        String filePath = writer.getFilePath();

                        // classify instances using the WekaRunner
                        WekaRunner wr = new WekaRunner();
                        wr.startFile(filePath);
                    } else {
                        throw new IllegalArgumentException("Line argument is not legal: Please use 10 values");
                    }
                } else {
                    throw new IllegalArgumentException("Line argument is not legal: Please only use digits");

                }

            }

        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * prints help
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Weka App", options);
    }

    @Override
    public String getFile() {
        return this.commandLine.getOptionValue(FILE);
    }

    @Override
    public String getLine() {
        return this.commandLine.getOptionValue(LINE);
    }
}
