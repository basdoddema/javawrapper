package nl.bioinf.bgdoddema;

import weka.classifiers.trees.RandomForest;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;

import java.io.File;

/**
 * this class uses a pre-made Vote algorithm to classify a newly given instance
 * this instance comes in csv file format and need to be uploaded by the user
 *
 * @author Bas
 */
public class WekaRunner {

    /**
     * method that classifies the newly given instances using the loaded model
     *
     * @param unknownFile file that contains the unclassified instances
     */
    public void startFile(String unknownFile) {

        try {
            // load pre-made model
            RandomForest loadedModel = loadClassifier();

            CSVLoader csvLoader = new CSVLoader();
            csvLoader.setSource(new File(unknownFile));
            // set attributes that will be used to perform the classification
            csvLoader.setNominalAttributes("first-last");
            Instances unknownInstancesFromCsv = csvLoader.getDataSet();

            if (unknownInstancesFromCsv.classIndex() == -1)
                unknownInstancesFromCsv.setClassIndex(unknownInstancesFromCsv.numAttributes() - 1);

            Instances newData = new Instances(unknownInstancesFromCsv);

            // add the class attribute that will be classified
            Add filter = new Add();
            filter.setAttributeIndex("last");
            filter.setNominalLabels("Bad,Good");
            filter.setAttributeName("quality");
            filter.setInputFormat(newData);
            newData = Filter.useFilter(newData, filter);

            // set the class index
            newData.setClassIndex(newData.numAttributes() - 1);

            // print the instances and attributes
            System.out.println(newData.numInstances());
            System.out.println("newData # attributes = " + newData.numAttributes());
            System.out.println("newData class index= " + newData.classIndex());

            System.out.println("unknownInstancesFromCsv = " + newData);

            // classifying new instances
            classifyNewInstance(loadedModel, newData);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method that classifies the new instances
     *
     * @param tree             the tree model
     * @param unknownInstances unclassified instances
     * @throws Exception
     */
    private void classifyNewInstance(RandomForest tree, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            Instance instance = unknownInstances.instance(i);
            double clsLabel = tree.classifyInstance(instance);
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
    }

    /**
     * Load the pre-made model
     *
     * @return The loaded random forest model
     * @throws Exception
     */
    private RandomForest loadClassifier() throws Exception {
        // deserialize model
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File("../../testdata/rf_model.model");
        return (RandomForest) weka.core.SerializationHelper.read(String.valueOf(file));
    }

}